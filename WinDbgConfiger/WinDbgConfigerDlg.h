
// WinDbgConfigerDlg.h: 头文件
//

#pragma once


// CWinDbgConfigerDlg 对话框
class CWinDbgConfigerDlg : public CDialogEx
{
// 构造
public:
	CWinDbgConfigerDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_WINDBGCONFIGER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	// windbg路径
	CString m_WinDbgPath;
private:
	void load_dbg_path();
public:
	afx_msg void OnBnClickedButton1();
private:
	// 执行Exe命令行
	bool RunExe(CString cmdline);
	bool RunExeAndWaitExit(CString cmdline);
	// 得到Windbg路径
	bool LoadWindbg(CString &retFile);
public:
	afx_msg void OnBnClickedButton2();
private:
	int PrintString(CString _string);
public:
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnBnClickedBtnok();
	afx_msg void OnBnClickedButton4();
private:
	bool is_host();
	DWORD_PTR find_process(LPCWSTR lpszName);
};
