
// WinDbgConfigerDlg.cpp: 实现文件
//

#include "stdafx.h"
#include "WinDbgConfiger.h"
#include "WinDbgConfigerDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CWinDbgConfigerDlg 对话框



CWinDbgConfigerDlg::CWinDbgConfigerDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_WINDBGCONFIGER_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CWinDbgConfigerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CWinDbgConfigerDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CWinDbgConfigerDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CWinDbgConfigerDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CWinDbgConfigerDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDOK, &CWinDbgConfigerDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CWinDbgConfigerDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDBTNOK, &CWinDbgConfigerDlg::OnBnClickedBtnok)
	ON_BN_CLICKED(IDC_BUTTON4, &CWinDbgConfigerDlg::OnBnClickedButton4)
END_MESSAGE_MAP()


// CWinDbgConfigerDlg 消息处理程序

BOOL CWinDbgConfigerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	// 读Windbg路径

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CWinDbgConfigerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CWinDbgConfigerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CWinDbgConfigerDlg::load_dbg_path()
{
	// TODO: 在此处添加实现代码.
	m_WinDbgPath = _T("");
	try
	{
		CRegKey KitKey;
		auto Open = KitKey.Open(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows Kits\\Installed Roots"));
		if (Open == ERROR_SUCCESS)
		{
			ULONG nCount = 0;
			TCHAR KitRoot10[MAX_PATH * 4] = {};
			nCount = ARRAYSIZE(KitRoot10);
			auto ls = KitKey.QueryStringValue(_T("KitsRoot10"), KitRoot10, &nCount);
			if (ls == ERROR_SUCCESS)
			{
				m_WinDbgPath = KitRoot10;
				m_WinDbgPath += _T("Debuggers\\x64\\windbg.exe");

			}
		}
	}
	catch (...)
	{

	}

}


void CWinDbgConfigerDlg::OnBnClickedButton1()
{
	//运行Windbg
	load_dbg_path();
	if (!::PathIsExe(m_WinDbgPath))
	{
		//从配置Load
		m_WinDbgPath = AfxGetApp()->GetProfileString(_T("Setting"), _T("WindbgPath"));
		if (!::PathIsExe(m_WinDbgPath))
		{
			//手工指定
			if (!LoadWindbg(m_WinDbgPath))
			{
				AfxMessageBox(_T("找不到Windbg"));
				return;
			}
		}
	}
	AfxGetApp()->WriteProfileString(_T("Setting"), _T("WindbgPath"), m_WinDbgPath);
	PrintString(m_WinDbgPath);
	CString cmdLine = _T("\"");
	cmdLine += m_WinDbgPath;
	cmdLine += _T("\" -b -k com:pipe,port=\\\\.\\pipe\\com_1,resets=0,reconnect");
	RunExe(cmdLine);
}
bool CWinDbgConfigerDlg::LoadWindbg(CString &retFile)
{
	CString strFile = _T("");
	CFileDialog	dlgFile(TRUE, _T("EXE"), _T("Windbg"));

	if (dlgFile.DoModal())
	{
		retFile = dlgFile.GetPathName();
		//AfxMessageBox(retFile);
		return ::PathIsExe(retFile);
	}
	return false;
}

// 执行Exe命令行
bool CWinDbgConfigerDlg::RunExe(CString cmdline)
{
	STARTUPINFO StartUpInfo = {};
	PROCESS_INFORMATION ProcessInfo = {};
	StartUpInfo.cb = sizeof(StartUpInfo);
	GetStartupInfo(&StartUpInfo);
	BOOL StartUpStatus = CreateProcess(NULL, cmdline.GetBuffer(0), NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &StartUpInfo, &ProcessInfo);
	if (StartUpStatus)
	{
		CloseHandle(ProcessInfo.hProcess);
		CloseHandle(ProcessInfo.hThread);
		return true;
	}
	return false;
}


void CWinDbgConfigerDlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	if (!is_host())
	{
		AfxMessageBox(_T("在Guest里设置Host?"));
	}
	//设置Host的SYM注册表
	//HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment
	//获取选择的DirSym
	CString DirSym = _T("D:\\Symbols");
	BROWSEINFO sInfo = {};
	sInfo.pidlRoot = 0;
	sInfo.lpszTitle = _T("请选择符号文件存储路径");
	sInfo.ulFlags = BIF_RETURNONLYFSDIRS | BIF_EDITBOX | BIF_DONTGOBELOWDOMAIN;
	sInfo.lpfn = NULL;
	TCHAR szFolderPath[MAX_PATH] = { 0 };
	// 显示文件夹选择对话框  
	LPITEMIDLIST lpidlBrowse = ::SHBrowseForFolder(&sInfo);
	if (lpidlBrowse != NULL)
	{
		// 取得文件夹名  
		if (::SHGetPathFromIDList(lpidlBrowse, szFolderPath))
		{
			DirSym = szFolderPath;
		}
	}
	if (lpidlBrowse != NULL)
	{
		::CoTaskMemFree(lpidlBrowse);
	}
	CString _valueKey = _T("_NT_SYMBOL_PATH");
	CString _value = _T("SRV*");
	_value += DirSym;
	_value +=_T("*http://msdl.microsoft.com/download/symbols");
	try
	{
		CRegKey HostKey;
		auto st = HostKey.Open(HKEY_LOCAL_MACHINE, _T("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment"));
		if (st == ERROR_SUCCESS)
		{
			st = HostKey.SetStringValue(_valueKey, _value);
			if (st == ERROR_SUCCESS)
			{
				//Set OK
				AfxMessageBox(_T("设置符号路径成功"));
				return;
			}
		}
	}
	catch (...)
	{

	}
	AfxMessageBox(_T("设置符号路径失败"));
}


int CWinDbgConfigerDlg::PrintString(CString _string)
{
	// TODO: 在此处添加实现代码.
	CDC* pDC = this->GetDC();
	pDC->SetBkMode(2);
	pDC->TextOut(0, 0, _string);
	this->ReleaseDC(pDC);
	return 0;
}

bool CWinDbgConfigerDlg::RunExeAndWaitExit(CString cmdline)
{
	STARTUPINFO StartUpInfo = {};
	PROCESS_INFORMATION ProcessInfo = {};
	StartUpInfo.cb = sizeof(StartUpInfo);
	GetStartupInfo(&StartUpInfo);
	BOOL StartUpStatus = CreateProcess(NULL, cmdline.GetBuffer(0), NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &StartUpInfo, &ProcessInfo);
	if (StartUpStatus)
	{
		WaitForSingleObject(ProcessInfo.hProcess, INFINITE);
		CloseHandle(ProcessInfo.hProcess);
		CloseHandle(ProcessInfo.hThread);
		return true;
	}
	return false;
}

void CWinDbgConfigerDlg::OnBnClickedButton3()
{
	if (is_host())
	{
		AfxMessageBox(_T("不能在Host里进行Guest设置"));
		return;
	}
	// TODO: 在此添加控件通知处理程序代码
	CString cmdline[] = {
		_T("cmd /c \"bcdedit /dbgsettings serial baudrate:115200 debugport:1 >c:\\1.txt\""),
		_T("cmd /c \"bcdedit /debug ON >c:\\2.txt\""),
		_T("cmd /c \"bcdedit -set TESTSIGNING ON >c:\\3.txt\"")
	};
	for (auto i = 0; i < ARRAYSIZE(cmdline); i++)
	{
		RunExeAndWaitExit(cmdline[i]);
	}
	AfxMessageBox(_T("设置Guest成功"));
}


void CWinDbgConfigerDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnOK();
}


void CWinDbgConfigerDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnCancel();
}


void CWinDbgConfigerDlg::OnOK()
{
	// TODO: 在此添加专用代码和/或调用基类

	CDialogEx::OnOK();
}


void CWinDbgConfigerDlg::OnCancel()
{
	// TODO: 在此添加专用代码和/或调用基类

	//CDialogEx::OnCancel();
}


void CWinDbgConfigerDlg::OnBnClickedBtnok()
{
	// TODO: 在此添加控件通知处理程序代码
}


void CWinDbgConfigerDlg::OnBnClickedButton4()
{
	CString strFile = _T("");
	CFileDialog	dlgFile(TRUE, _T(".vmx"));

	if (dlgFile.DoModal())
	{
		strFile = dlgFile.GetPathName();
		//判断文件存在
	}
	if (!PathFileExists(strFile))
	{
		AfxMessageBox(_T("文件不存在"));
		return;
	}

	CStdioFile file(strFile, CFile::modeReadWrite);
	CString line = _T("");
	CString s = _T("");
	while (file.ReadString(line))
	{
		OutputDebugString(line);
		OutputDebugString(_T("\n"));
		//PrintString(line);
		s += line;
		s += TEXT("\r\n");
	}
	file.SeekToEnd();
	CString WinVmx[] = {
		_T("\nserial0.present = \"TRUE\"\n"),
		_T("serial0.yieldOnMsrRead = \"TRUE\"\n"),
		_T("serial0.fileType = \"pipe\"\n"),
		_T("serial0.fileName = \"\\\\.\\pipe\\com_1\"\n"),
		_T("serial0.tryNoRxLoss = \"TRUE\"\n")
	};
	for (auto i = 0; i < ARRAYSIZE(WinVmx); i++)
	{
		file.WriteString(WinVmx[i]);
	}
	//file.WriteString();
	file.Close();
	AfxMessageBox(_T("修改VMX成功"));
	
}


bool CWinDbgConfigerDlg::is_host()
{
	// TODO: 在此处添加实现代码.
	BOOLEAN old;
	auto status = ntdll::RtlAdjustPrivilege(SE_PROF_SINGLE_PROCESS_PRIVILEGE, TRUE, FALSE, &old);
	status |= ntdll::RtlAdjustPrivilege(SE_DEBUG_PRIVILEGE, TRUE, FALSE, &old);
	if (!NT_SUCCESS(status))
		return false;
	//vmware-tray.exe
	auto findpid = find_process(L"vmware-tray.exe");
	if (findpid)
		return true;
	return false;
}


DWORD_PTR CWinDbgConfigerDlg::find_process(LPCWSTR lpszName)
{
	DWORD_PTR dwPid = 0;
	unsigned long cbBuffer = 0x5000;  //Initial Buffer Size
	void* Buffer = (void*)LocalAlloc(0, cbBuffer);
	if (Buffer == 0) return 0;
	bool x = false;
	bool error = false;
	while (x == false)
	{
		int ret = ntdll::NtQuerySystemInformation(ntdll::SystemExtendedProcessInformation, Buffer, cbBuffer, 0);
		if (ret < 0)
		{
			if (ret == STATUS_INFO_LENGTH_MISMATCH)
			{
				cbBuffer = cbBuffer + cbBuffer;
				LocalFree(Buffer);
				Buffer = (void*)LocalAlloc(0, cbBuffer);
				if (Buffer == 0) return 0;
				x = false;
			}
			else
			{
				x = true;
				error = true;
			}
		}
		else x = true;
	}
	if (error == false)
	{
		ntdll::SYSTEM_PROCESS_INFORMATION* p = (ntdll::SYSTEM_PROCESS_INFORMATION*)Buffer;
		while (1)
		{
			WCHAR szName[MAX_PATH] = { 0 };
			__try
			{
				RtlCopyMemory(szName, p->ImageName.Buffer, min(p->ImageName.MaximumLength, 512));
				if (_wcsicmp(PathFindFileName(szName), lpszName) == 0)
				{
					auto pid = reinterpret_cast<DWORD_PTR>(p->UniqueProcessId);
					dwPid = pid;
					break;
				}
			}
			__except (1)
			{
				return 0;
			}
			if (p->NextEntryOffset == 0) break;
			p = (ntdll::SYSTEM_PROCESS_INFORMATION*)((unsigned char*)p + (p->NextEntryOffset));
		}
	}
	LocalFree(Buffer);
	return dwPid;
}
